1. Развернул две виртуалки keycloak01 и db01 на Vagrant.
2. Установил Postgresql на db01, создал базу keycloak, юзера к ней, выдал привелегии.
3. Установил сервер Keycloak на виртуалку keycloak01
4. Создал тестовый realm и юзера в нем
5. Создал хост keycloak02 на Vagrant, запустил keycloak в режиме кластера
6. Создал ansible роль для установки postgresql и создания базы и пользователя.
7. Создал ansible роль для установки keycloak в кластере.
8. Вынес ansible роли в отдельный репозиторий https://gitlab.com/deusops-training/ansible-roles для установки через ansible-galaxy.
